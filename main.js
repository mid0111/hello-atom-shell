var app = require('app');
var BrowserWindow = require('browser-window');
var Menu = require('./menu');

// Report crashes to our server.
require('crash-reporter').start();

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the javascript object is GCed.
var mainWindow = null;

// loadMenu
Menu.loadMenu();

// Quit when all windows are closed.
app.on('window-all-closed', function() {
  if(process.platform != 'drawin') {
    app.quit();
  }
});

// Ready to create browser windows.
app.on('ready', function() {
  // Create tha browser window.
  mainWindow = new BrowserWindow({width: 800, height: 600});
  mainWindow.loadUrl('file://' + __dirname + '/index.html');
  global.window = mainWindow;

  // Emitted when the window is closed.
  mainWindow.on('closed', function() {
    mainWindow = null;
  });
});

