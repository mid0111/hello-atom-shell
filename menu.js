var app = require('app');
var Menu = require('menu');
var _ = require('underscore-plus');

// var focusedWindow = function() {
//   _.find(global.atomApplication.windows, function(atomWindow) {
//     return atomWindow.isFocused();
//   });
// };


// Menu
exports.loadMenu = function(){
  var template = [
    {
      label: 'File',
      submenu: [
        {
          label: 'Quit',
          accelerator: 'Command+Q',
          click: function() { app.quit(); }
        }
      ]
    },
    {
      label: 'Window',
      submenu: [
        {
          label: 'Maximize',
          accelerator: 'F11',
          click: function(){
            global.window.maximize();
          }
        }
      ]
    }
  ];

  var menu = Menu.buildFromTemplate(template);
  Menu.setApplicationMenu(menu);
};

